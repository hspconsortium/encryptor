#!/usr/bin/env bash

echo "Note: this should be run from the project root directory"

profile="local"

if [ $# -gt 0 ]; then
    if [ "$1" == "local" ] || [ "$1" == "test" ] || [ "$1" == "prod" ]; then
        profile=$1
    else
        echo "usage: $0 {local | test | prod}";
        exit 1;
    fi
fi

export SPRING_PROFILES_ACTIVE="${profile}"

echo "SPRING_PROFILES_ACTIVE=${SPRING_PROFILES_ACTIVE}"

echo "starting encryptor..."

# from the environment
echo "JASYPT_ENCRYPTOR_PASSWORD=${JASYPT_ENCRYPTOR_PASSWORD}"

. docker/build.sh

# run the container, using the arguments from the hosting environment
docker run -i -e "SPRING_PROFILES_ACTIVE=${SPRING_PROFILES_ACTIVE}" -e "JASYPT_ENCRYPTOR_PASSWORD=${JASYPT_ENCRYPTOR_PASSWORD}" -p 8088:8088 -t hspconsortium/encryptor:latest
