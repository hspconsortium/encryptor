package org.hspconsortium;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EncryptorApp {

	public static void main(String[] args) {
		SpringApplication.run(EncryptorApp.class, args);
	}
}
