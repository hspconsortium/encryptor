package org.hspconsortium.service;

import io.apigee.trireme.core.*;
import org.jasypt.encryption.StringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutionException;

@Service
public class EncryptionService {

    @Autowired
    private StringEncryptor encryptor;

    @Autowired
    private ResourceLoader resourceLoader;

    @Value("${jasypt.encryptor.password}")
    private String encryptorPassword;

    public String encryptForJava(String input) {
        return encryptor.encrypt(input);
    }

    /**
     * The following is aiming at getting Nodejs running on the JVM, then capturing output from the executing
     * node app. From what I can tell, the only way to capture that output is to intercept the standard
     * output stream.
     * <p>
     * The executing nodejs app logs the encrypted value to the console, which is captured and returned from this java
     * method.
     */
    public String encryptForNodejs(String input) {

        PrintStream soutHolder = System.out;
        String result = null;
        try {
            // read in the script to execute
            Resource resource = resourceLoader.getResource("classpath:nodejsServerScripts/encrypt.js");
            InputStream inputStream = resource.getInputStream();
            byte[] bdata = FileCopyUtils.copyToByteArray(inputStream);
            String script = new String(bdata, StandardCharsets.UTF_8);

            // execute script and capture output
            NodeEnvironment nodeEnvironment = new NodeEnvironment();
            NodeScript nodeScript = nodeEnvironment.createScript("encrypt.js", script, new String[]{encryptorPassword, input});
            nodeScript.setNodeVersion("0.10");

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PrintStream ps = new PrintStream(baos, true, "utf-8");

            System.setOut(ps);

            ScriptFuture scriptFuture = nodeScript.execute();
            ScriptStatus scriptStatus = scriptFuture.get();

            String content = new String(baos.toByteArray(), StandardCharsets.UTF_8);
            ps.close();

            // return result
            if (content.endsWith("\n")) {
                result = content.substring(0, content.length() - 1);
            } else {
                result = content;
            }

        } catch (NodeException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.setOut(soutHolder);
        }

        return result;
    }
}
