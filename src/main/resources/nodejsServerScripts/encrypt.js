var crypto = require('crypto');

var algorithm = 'aes-256-ctr';
var password = process.argv[2];
var message = process.argv[3];

function encrypt(text) {
    var cipher = crypto.createCipher(algorithm, password);
    var crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
}

console.log(encrypt(message));
