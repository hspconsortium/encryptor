function encryptForJava(text){
    var data = {
        "in": text
    };

    $.ajax({
        type: 'POST',
        url: '/encryptForJava',
        data: JSON.stringify(data),
        success: encryptForJavaResultHandler,
        contentType: "application/json",
        dataType: 'json'
    });

}

function encryptForJavaResultHandler(data){
    $('#javaSecretEncrypted').html(data.out);
}

function encryptForNodejs(text){
    var data = {
        "in": text
    };

    $.ajax({
        type: 'POST',
        url: '/encryptForNodejs',
        data: JSON.stringify(data),
        success: encryptForNodejsResultHandler,
        contentType: "application/json",
        dataType: 'json'
    });
}

function encryptForNodejsResultHandler(data){
    $('#nodejsSecretEncrypted').html(data.out);
}