# Encryption Utility

The encryption utility is a simple SpringBoot application which utilizes the
`jasypt-spring-boot-starter` dependency to encrypt text.

The application exposes an API endpoint at `POST: /encrypt` which expects JSON in the
format:

```json
{
  "in": "someTextToEncrypt"
}
```


The result of the call is the JSON:

```json
{
  "in": "someTextToEncrypt",
  "out": "someTextEncrypted"
}
```

where `someTextEncrypted` is the result of encrypting `someTextToEncrypt` with the 
algorithm `PBEWITHMD5ANDTRIPLEDES` and using the password defined by an environment
variable `JASYPT_ENCRYPTOR_PASSWORD`.  In order for the application to run correctly,
that environment variable must be set and available to the app.